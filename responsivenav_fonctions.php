<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

// On définit la classe par défaut ajoutée par le plugin.
// Surchargeable dans squelettes/mes_fonctions.php
define('_RESPONSIVENAV_JSCLASS', 'js-responsivenav');
